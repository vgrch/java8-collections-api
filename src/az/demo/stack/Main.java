package az.demo.stack;

import java.util.Stack;

public class Main {

    public static void main(String[] args) {
	// write your code here

        Stack javaStack = new Stack<>();
        az.demo.stack.usingLinkedList.Stack myStackll = new az.demo.stack.usingLinkedList.Stack();
        az.demo.stack.usingArray.Stack myStackArr = new az.demo.stack.usingArray.Stack();


        javaStack.push("item1");
        javaStack.push("item2");
        javaStack.push("item3");
        javaStack.push("item4");


        myStackArr.push("item1");
        myStackArr.push("item2");
        myStackArr.push("item3");
        myStackArr.push("item4");

        myStackll.push("item1");
        myStackll.push("item2");
        myStackll.push("item3");
        myStackll.push("item4");

        System.err.println(javaStack);
        System.err.println(myStackll);
        System.err.println(myStackArr);

        System.err.println(javaStack.pop());
        System.err.println(myStackll.pop());
        System.err.println(myStackArr.pop());

        System.err.println(javaStack.peek());
        System.err.println(myStackll.peek());
        System.err.println(myStackArr.peek());

        System.err.println(javaStack);
        System.err.println(myStackll);
        System.err.println(myStackArr);
    }
}
