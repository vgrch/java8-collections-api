package az.demo.stack.usingArray;

import java.util.Arrays;
import java.util.EmptyStackException;

public class Stack<T> {

    private T[] data = (T[]) new Object[2];
    int indexOfLast = -1;

    public boolean push(T item) {
        if (indexOfLast + 1 >= data.length) expandCapacity();
        data[++indexOfLast] = item;
        return true;
    }

    private void expandCapacity() {
        int length = data.length;
        T[] temp = (T[]) new Object[length + length / 2];

        for (int i = 0; i < data.length; i++) {
            temp[i] = data[i];
        }
        data = temp;
        temp = null;
    }

    public T pop() {
        if (isEmpty()) throw new EmptyStackException();
        T datum = data[indexOfLast--];
        data[indexOfLast+1] = null;
        return datum;
    }

    public T peek() {
        if (isEmpty()) throw new EmptyStackException();
        return data[indexOfLast];
    }

    public int getSize() {
        return indexOfLast;
    }

    public boolean isEmpty() {
        return indexOfLast == -1;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("[");
        for (int i = 0; i < data.length; i++) {
            if (data[i] == null) break;

            stringBuilder.append(data[i]);
            stringBuilder.append(", ");
        }
        stringBuilder.deleteCharAt(stringBuilder.lastIndexOf(","));
        stringBuilder.deleteCharAt(stringBuilder.lastIndexOf(" "));
        stringBuilder.append("]");
        return stringBuilder.toString();
    }

}
