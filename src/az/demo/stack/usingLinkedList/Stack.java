package az.demo.stack.usingLinkedList;

import java.util.EmptyStackException;
import java.util.LinkedList;

public class Stack<T> {

    private final LinkedList<T> ll = new LinkedList<>();

    public boolean push(T element) {
        ll.add(element);
        return true;
    }

    public T pop() {
        if (isEmpty()) throw new EmptyStackException();
        return ll.removeLast();
    }

    public T peek() {
        if (isEmpty()) throw new EmptyStackException();
        return ll.getLast();
    }

    public int getSize() {
        return ll.size();
    }

    public boolean isEmpty() {
        return ll.size() == 0;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder(ll.toString());
        return sb.toString();
    }
}
