package az.demo.tree.binaryTree;

// binary search tree java implementation
// https://www.youtube.com/watch?v=oSWTXtMglKE

// insert delete lookup big O notation: worst O(n) good O(log n)
// to prevent worst case use balanced binary search tree

public class BinarySearchTree {

    private Node root;

    public void insert(int value) {
        if (root == null) root = new Node(value);
        else {
            root.insert(value);
        }
    }

    public void print() {
        if (root != null) root.printInOrder();
    }

    public static void main(String[] args) {
        BinarySearchTree bst = new BinarySearchTree();

        bst.insert(5);
        bst.print();
        System.out.println();

        bst.insert(6);
        bst.print();
        System.out.println();

        bst.insert(7);
        bst.print();
        System.out.println();

        bst.insert(8);
        bst.print();
        System.out.println();

        bst.insert(3);
        bst.print();
        System.out.println();

        bst.insert(4);
        bst.print();
    }

}

class Node {
    private Node left, right;
    private int value;

    public Node(int value) {
        this.value = value;
        this.left = null;
        this.right = null;
    }


    public void insert(int value) {
        Node newNode = new Node(value);

        if (value < this.value) {
            if (left == null) left = newNode;
            else left.insert(value);
        } else {
            if (right == null) right = newNode;
            else right.insert(value);
        }

    }

    public boolean contains(int value) {
        if (value == this.value) return true;

        if (this.value < value) {
            if (right == null) return false;
            else return right.contains(value);
        } else {
            if (left == null) return false;
            else return left.contains(value);
        }
    }

    public void printInOrder() {
        if (left != null)
            left.printInOrder();
        System.out.print(value + ", ");
        if (right != null)
            right.printInOrder();
    }


}
