package az.demo.linkedlist;

import java.util.LinkedList;

public class JavaLinkedList {
    Node head;
    int size;

    public JavaLinkedList() {
        head = null;
        size = 0;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public Node add(int data) {
        Node newNode = new Node(data, head);
        this.head = newNode;
        this.size++;
        return newNode;
    }

    public Node find(int data) {
        Node thisNode = this.head;
        while (thisNode != null) {
            if (thisNode.getData() == data) return thisNode;
            thisNode = thisNode.getNextNode();
        }
        return null;
    }

    public boolean remove(int data) {
        Node thisNode = this.head;
        Node prevNode = null;

        while (thisNode != null) {
            if (thisNode.getData() == data) {
                prevNode.setNextNode(thisNode.getNextNode());
                this.setSize(this.getSize() - 1);
                return true;
            }
            prevNode = thisNode;
            thisNode = thisNode.getNextNode();
        }
        return false;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("MyLinkedList{\n");
       Node node = head;
        while (node!= null) {
            sb.append(node.getData() + "\n");
            node = node.getNextNode();
        }
        sb.append('}');
        return sb.toString();
    }

    public static void main(String[] args) {
            JavaLinkedList jll = new JavaLinkedList();
            jll.add(1);
            jll.add(111);
            jll.add(11);
            jll.add(12);
            jll.add(5);
        System.out.println(jll.toString());

    }

}

class Node {

    private int data;
    private Node nextNode;

    public Node() {
    }

    public Node(int data) {
        this.data = data;
    }

    public Node(int data, Node nextNode) {
        this.data = data;
        this.nextNode = nextNode;
    }

    public int getData() {
        return data;
    }

    public void setData(int data) {
        this.data = data;
    }

    public Node getNextNode() {
        return nextNode;
    }

    public void setNextNode(Node nextNode) {
        this.nextNode = nextNode;
    }
}
