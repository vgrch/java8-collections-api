package az.demo.InterfaceList;

import java.util.LinkedList;

public class ClassLinkedList {


    // LinkedList store elements as nodes
    // node contains 3 data , element value previous and next node
    static LinkedList<String> linkedList = new LinkedList();

    public static void main(String[] args) {


        // add or remove elements linked list faster than array list
        // slow for search operations
        linkedList.add("String1");
        linkedList.add("String2");
        linkedList.add("String3");
        System.out.println(linkedList);


    }

}
