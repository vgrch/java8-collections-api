package az.demo.InterfaceList;

import java.util.Stack;

public class ClassStack {

    static Stack<String> stack = new Stack<>();

    public static void main(String[] args) {

        stack.push("String1");
        System.out.println(stack);
        stack.push("String2");
        System.out.println(stack);
        stack.push("String3");
        System.out.println(stack);
        stack.push("String4");
        System.out.println(stack);
        stack.push("String5");
        System.out.println(stack);

        System.out.println(stack.peek());
        System.out.println(stack.pop());
        System.out.println(stack);
        // last in first out : LIFO
    }
}
