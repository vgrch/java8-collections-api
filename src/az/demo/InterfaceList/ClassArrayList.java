package az.demo.InterfaceList;

import com.sun.org.apache.xpath.internal.SourceTree;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class ClassArrayList {

    // ArrayList implements List interface
    // allow dynamically add or remove elements
    // use internal array to store elements background
    //  ArrayList is implemented as a resizable array

    // It is a good habit to construct the ArrayList with a higher initial capacity. This can avoid the resizing cost.
    static ArrayList<String> arr_list = new ArrayList<>(30);

    // it's time consuming add or remove elements ArrayList slower than LinkedList
    // but get and set (update) is high performance faster than LinkedList
    // search is faster than LinkedList

    public static void main(String[] args) {
        // add element end of list
        System.out.println("Adding String1 ");
        arr_list.add("String1");
        System.out.println("Adding String2 ");
        arr_list.add("String2");
        System.out.println("Adding String3 ");
        arr_list.add("String3");
        System.out.println(arr_list);

        // Classes that implements from list interface can contain duplicate values
        // string pooling
        System.out.println("Adding String3 that already exist");
        arr_list.add("String3");
        System.out.println(arr_list);

        // update element in the list
        arr_list.set(3, "String4");
        System.out.println("After update element on the position 3 : " + arr_list);

        // add elements middle or beginning of list
        System.out.println("Adding String0 positin 0 element ");
        arr_list.add(0, "String0");
        System.out.println("Adding String1 positin 1 element ");
        arr_list.add(1, "String1");
//        System.out.println("Adding String6 positin 10 element ");
//        arr_list.add(10, "String6"); // will threw IndexOutOfBoundsException

        //remove element by index
        System.out.println("Before remove 1 : " + arr_list);
        arr_list.remove(1);
        System.out.println("After remove 1 : " + arr_list);

        // remove element using iterator
        Iterator<String> stringIterator = arr_list.iterator();
        while (stringIterator.hasNext()){
            if (stringIterator.next().equals("String4")) stringIterator.remove();
        }
        System.out.println("After remove 'String4' using iterator : " + arr_list);

        // add another list
        List<String> sub_list = new ArrayList<>();
        sub_list.add("String4");
        sub_list.add("String5");

        System.out.println("Sub list : " + sub_list);

        arr_list.addAll(sub_list);
        System.out.println("After adding sublist : " + arr_list);

        // remove all elements form the list
        sub_list.clear();
        System.out.println("After clear invoke sublist : " + sub_list);

        // check if the specified element exists in the given arraylist
        // String pooling. with new keyworn it never return true
        System.out.println("Is 'String1' exist in the list? : " + arr_list.contains("String1"));
        System.out.println("Is 'String10' exist in the list? : " + arr_list.contains("String10"));

        // return number of elements it contains
        arr_list.size();
        System.out.println("Size of list : " + arr_list.size());

        // get element by index
        System.out.println("Element that index is 1 : " + arr_list.get(1));

        // get index of object
        System.out.println("Index of 'String1' is : " + arr_list.indexOf("String1"));


        // for each using lambda expression since 1.8
        arr_list.forEach(s -> System.out.println(s));

        // loop array with basic for loop
        System.out.println("Loop array with basic for loop");
        for (int i = 0; i < arr_list.size(); i++) {
            System.out.println(arr_list.get(i));
        }

        // loop array with enhanced for loop
        System.out.println("Loop array with enhanced for loop");
        for(String s:arr_list){
            System.out.println(s);
        }

        // loop using iterator
        System.out.println("Loop using iterator");
        Iterator<String> iterator = arr_list.iterator();
        while (iterator.hasNext()) System.out.println(iterator.next());

        //

        Collections.reverse(arr_list);
        System.out.println("After reverse of list : " +  arr_list);

        Collections.sort(arr_list);
        System.out.println("After sort list : " + arr_list);

    }
}